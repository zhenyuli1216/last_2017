This repo consists of codes and text data to replicate the results in the paper:

Mining Aspect-Specific Opinion using a Holistic Lifelong Topic Model. Shuai Wang, Zhiyuan Chen, Bing Liu. WWW, 2016.

# Basic assumptions:
+ An aspect is represented by a topic.

+ Each aspect has its own opinion words.

+ All domains share the same vocabulary (the paper does not state this explicitly).

# Code design

## Data structures

We have the following matrices to keep track of the corresponding counts.
The matrix names are consistent with those in the paper.

n_dl: 

![picture](LAST/img/n_dl.png)

n_dkl:

![picture](LAST/img/n_dkl.png)

n_klv:

![picture](LAST/img/n_klv.png)

n_lvc:

![picture](LAST/img/n_lvc.png)

n_klvc:

![picture](LAST/img/n_klvc.png)

## The JAST Algorithm

This algorithm uses Gibbs sampling to sample the above 5 matrices.

> for each word w_i in each document d
> > sample from equaiton (1)
> > create topic-sentiment distribution A = L * M * R (element-wise multiplication)
> > > A is of size (T, S) with A_kl = P(z_i = k, s_i = l | conditions)

> > > L is a vector of length (T, S), L_kl = n_dl* + \gamma / \sum_x (n_dx* + \gamma)

> > > M is a matrix of size (T, S), M_kl = n_dkl* + \alpha / \sum_x (n_dxl* + \alpha)

> > > R is a matrix of size (T, S), R_kl = n_klv* + \beta / \sum_x (n_klx* + \beta)

L is just the d-th row of n_dl, properly smoothed and normalized.

M is the two sub-vectors in the d-th rows in the two respective sentiment blocks from n_dkl (see the two colored strips in n_dkl),
normalized within each sentiment over all topics and then juxtaposed.

R is the two sub-vectors in the v-th columns in the two respective sentiment blocks from n_klv (see the two colored strips in n_dkl),
normalized within each sentiment over all topics.

The columns are transposed and juxtaposed.

After a pair (k,l) is sampled for the word, create a vector B of length 3 to sample the word type from equation (2):
> create the word-type distribution B \propto g * C
> > g(c,w_i) = 1 if w_i is in the lexicon and c is an aspect-specific or general opinion word.

> > g(c,w_i) = 1 if w_i is not in the lexicon and c is an aspect word.

> > g(c,w_i) = 0 for any other cases.

> > C is a vector of length 3, C(2, w_i) = n_lvc* + \beta / \sum_x (n_lxc* + \beta) [the probability that w_i is of sentiment l and is a general sentiment word]

> > C(c, w_i) = n_klvc* + \beta / \sum_x (n_klxc* + \beta) [the probability that w_i is of sentiment l and topic k and is of word type c.]


\phi^G_s is a probability distribution over the vocabulary about how likely each word is used as a general opinion word for sentiment s.

This is obtained from a the 3rd row of the s-th sentiment block in n_lvc.

\phi^A_{s,k} is obtained from the k-th row of the s-th sentiment (outer) block and first word type (inner) block in n_klvc.

\phi^O_{s,k} is obtained from the k-th row of the s-th sentiment (outer) block and second word type (inner) block in n_klvc.

After JAST finishes the sampling, each aspect (in **A**) is represented by top 15 words.

An aspect-specific opinion (in **O**) is also a list of 15 most relevant words.

**G** for a domain is represented by a list of 25 most relevant words.


Parameter | Value
--- | --- | ---
Total iterations | 1000 + 200
\alpha | 0.1
\beta | 0.01
\gamma | 1
T | 15
\lambda^{A,O} | 1
\pi | 7
\mu | 0.3
FIM min sup (aspect-opinion) | max(4,0.7 x (size of D_s))
FIM min sup (aspect-aspect) | max(4,0.3 x (size of D_s))
FIM min sup (opinion-opinion) | max(4,0.2 x (size of D_s))


### Monitoring the sampling processing
We can use perplexity, as in the original LDA paper, to check how much progress we've made.
However, \log P(**w**| \Phi), which is needed in perplexity,
is not easy to calculate and approximation is needed. See this [paper](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.149.771&rep=rep1&type=pdf)
for a few ways to calculate the log likelihood.

### Averaging multiple MCMC chains
See this [paper](www.aclweb.org/anthology/D14-1182).

## The LAST Algorithm

### Step 1: Aspect Mining and Matching

The algorithm first runs JAST for all domains and obtains the corresponding count matrices.
An important step (line 6 in Algorithm 1) is to find in the target domain (i) a topic/aspect that is most similar to each aspect in an auxiliary domain j.
The word distribution of a sentiment (s=0,1) under each topic t(i) in domain i
is compared with that of the same sentiment under the topic t(j) in domain j.
The word distributions are rows in the n_klv matrices in both domains (that is why all domains need to have the same vocabulary).

Let t(i)* be the topic closest to topic t(j).
If the similarity between t(i)* and t(j) is sufficient (<pi),
in line 8 of Algorithm 1,
the words expressing aspect t(j) under sentiment s from domain j (the top 15 words retrieved from the t(j)-th row in the 1st inner block in the s-th outer block of n_klvc),
along with the opinion words expressing the same sentiment on aspect t(j)
(top 15 words read out from the t(j)-th row in the 2nd inner block in the s-th outer block of n_klvc),
are transferred to topic t(i)* under sentiment s in domain i.

### Step 2: Knowledge mining

The so-called knowledge is 3 types of word associations:
1) aspect-opinion pairs, 2) aspect-aspect pairs, and 3) opinion-opinion pairs.
These associations can be considered as a graph of words connected by logical relations.
For a pair of words to qualify as reliable knowledge for domain i,
the pair has to appear sufficiently frequent in the auxiliary domains.
For each sentiment s and each topic/aspect t(i) in the target domain i,
the set of frequent itemsets of length 2 (two words) in the set S_{s,t(i)}
are mined with minimum support requirements specified in the following parameter table.
For the frequent itemset mining (FIM) algorithm, see reference [1] in the paper.
Using the terminology of frequent itemset mining,
a pair is considered as to have occurred once if the pair was resulted from
a matched topic t(j) under sentiment s from any auxiliary domain.
Of course, the pair can occur multiple times.

### Step 3: Knowledge Utilization (lines 18-20)

This step essentially use the (filtered) mined word graph
as supervision when learning another JAST model for the target domain.
If a word in the graph is regarded as a general opinion word (e.g., nice)
in domain i, the word and the associated edges will not be used as supervision.

Generalized Polya Urn (GPU) modifies the Gibbs sampling equation (1)
with a promotation matrix P(w,v) where w and v are two words.
See Eqs. (5) - (7) to calculate P (with hyper-parameter mu).
The modified Eq. (1) is shown in Eq. (9).
Eq. (2) is used to sample word type.
