"""
	Implement the model in
		Mining Aspect-Specific Opinion using a Holistic Lifelong Topic Model, WWW, 2016
"""
import sys
import os
import shutil
import re 
import pickle

import numpy as np
import scipy as sp
from scipy.special import gammaln
from scipy import misc
import math
from numpy.random import RandomState 
prng = RandomState(1216)

def word_indices(word_frequencies):
	"""
		input: a document vector represented as (word_id, frequency)
		output: a sequence of word indices
	"""
	# for each word (k = word_id)
	for k, v in word_frequencies.items():

		# generate the word_id v times
		for i in range(int(v)):
			yield k

def sample_index(p):
	"""
		Sample from the Multinomial distributoin and return the sample index 
		p must be normalized to a probability distribution
	"""
	b = p/p.sum()
	return prng.multinomial(1,b).argmax()  

def find_word(code):
	word = dic[code]
	if word in lexicon: 
		return 1
	else: 
		return 0 

myNegativeSet = set(line.strip().lower() for line in open('../data/negative-words.txt', encoding = "ISO-8859-1"))
myPositiveSet = set(line.strip().lower() for line in open('../data/positive-words.txt', encoding = "ISO-8859-1"))
lexicon = myNegativeSet.union(myPositiveSet)

class JAST(object):
	def __init__(self, n_topics, n_sentiments, n_wordTypes, alpha=0.1, beta=0.01, gamma=1):
		"""
			n_topics: desired number of topics
			n_sentiments: desired number of sentiments (2)
			n_wordTypes: number of word types (3)
			alpha: a scalar
			beta: a scalar
			gamma: a scalar
		"""
		self.n_topics = n_topics
		self.n_sentiments = n_sentiments 
		self.n_wordTypes = n_wordTypes
		self.alpha = alpha
		self.beta = beta
		self.gamma = gamma

	def _initialize(self, corpus, n_docs, vocab_size):
		"""
			Randomly 
		"""

		self.n_docs = n_docs
		self.vocab_size = vocab_size

		# topic distribution
		self.nz = np.zeros(self.n_topics)

		# sentiment distribution
		self.ns = np.zeros(self.n_sentiments)

		# sentiment distribution of each document
		self.nms = np.zeros((n_docs, self.n_sentiments))

		# joint topic-sentiment distribution of each document
		self.nmzs = np.zeros((n_docs, self.n_topics*self.n_sentiments))

		# joint topic-sentiment distribution of each word
		self.nzsw = np.zeros((self.n_topics * self.n_sentiments, vocab_size))

		# joint topic-wordtype distribution of each word
		self.nswc = np.zeros((self.n_sentiments*self.n_wordTypes, vocab_size))

		# joint topic-sentiment-wordtype distribution of each word
		self.nzswc = np.zeros((self.n_topics*self.n_sentiments*self.n_wordTypes, vocab_size))

		# topics / sentiments / wordtypes of each word in each document
		# key = (doc_id, position of the word within the document)
		self.topics = {}
		self.sentiments = {}
		self.wordTypes = {}

		# m - doc_id
		# v - words frequencies in the doc
		for m, v in corpus.items():
			# for each word
			for i, w in enumerate(word_indices(v)):
				# choose an arbitrary topic as first topic for word i
				z = prng.randint(0, self.n_topics)
				s = prng.randint(0, self.n_sentiments)
				c = prng.randint(0, self.n_wordTypes)
				self.nz[z] += 1
				self.ns[s] += 1
				self.nms[m,s] += 1

				self.topics[(m,i)] = z
				self.sentiments[(m,i)] = s
				self.wordTypes[(m,i)] = c

				self.nmzs[m, s * self.n_topics + z] += 1
				self.nzsw[s * self.n_topics + z, w] += 1
				self.nswc[s * self.n_wordTypes + c, w] += 1
				self.nzswc[(self.n_wordTypes * s + c) * self.n_topics + z, w] += 1

	def _conditional_distribution(self, m, w):
		"""
			Construct the joint probability P(topic z and sentiment s | z', s', w, alpha, beta, gamma) used in Eq. (1) in the paper.
			Args:
				m: document id
				w: word id
			Return:
				a 2D matrix representing p(z,s) without normalization
		"""
		leftGamma = self.nms[m, :] + self.gamma	# this is just two numbers
		left = leftGamma/leftGamma.sum()
		L = np.tile(left, (self.n_topics, 1))	# an n_topics x n_sentiments matrix
		
		topicAlpha = self.nmzs[m, :] + self.alpha	# 2 x n_topics row vector
		
		middle_s1 = topicAlpha[:self.n_topics]	# topic distribution of the first sentiment
		middle_s2 = topicAlpha[self.n_topics:]	# topic distribution of the second sentiment
		middle1 = middle_s1/middle_s1.sum()    # normalize within each sentiment
		middle2 = middle_s2/middle_s2.sum()
		
		#M = np.concatenate((middle1, middle2), axis = 0).reshape(self.n_sentiments, self.n_topics).transpose()
		M = np.vstack((middle1, middle2)).transpose()	# to an n_topics x n_sentiments matrix
		#print ('M' , M)

		# 2 x n_topics column vector
		wordBeta = self.nzsw[:, w] + self.beta
		wordSum = self.nzsw.sum(axis=1) + self.vocab_size * self.beta
		right = wordBeta / wordSum     # SUM OVER THE ENTIRE M
		R = right.reshape(self.n_topics, self.n_sentiments)		# n_topics x n_sentiments matrix
		#print ('R' , R)
		#print (R.shape)
		
		p_z_s = L * M * R
		
		return p_z_s

	def _wordType_distribution(self, x, w, s, z):
		"""
			Construct the distributioni of word type
				P(r_i = c | x, z, s, w, alpha, beta, gamma) used in Eq. (2) in the paper.
			Args:
				x: indicator of whether w is in the sentimental lexicon
				w: word id
				s: sentiment of w
				z: topic of w
			Return:
				a vector of length 3 representing p(r_i = c) without normalization
		"""
	
		# w is not in the sentimental lexicon
		if x == 0:
			return np.array([1,0,0])
		
		# if x is in the lexicon
		
		# aspect-specific opinion
		c = 1
		# go to a specific combination of (sentiment, word_type, topic)
		scz_index = ((s * self.n_wordTypes) + c) * self.n_topics + z
		topBeta = self.nzswc[scz_index, w] + self.beta
		wordSum= self.nzswc[scz_index, :].sum(axis = 0) + self.vocab_size * self.beta
		top = topBeta / wordSum
		
		# general opinion
		c = 2
		# go to a specific combination of (sentiment, word_type)
		sc_index = s * self.n_wordTypes + c
		bottomBeta = self.nswc[sc_index, w] + self.beta
		wordSum = self.nswc[sc_index, :].sum(axis = 0) + self.vocab_size * self.beta
		bottom = bottomBeta / wordSum
		
		return np.array([0, top, bottom])

	def phi_G_s(self): # p(G|s)
		"""
			Construct general opinion word distribution for each sentiment
			Return:
				a 2 x vocab_size matrix, representing topic distribution over words in two sentiments.
		"""
		c = 2 # general opinion word
		
		# negative sentiment word distribution
		# third row
		s0 = self.nswc[0 * self.n_wordTypes + c, :]
		
		# positive sentiment word distribution
		# fifth row
		s1 = self.nswc[1 * self.n_wordTypes + c, :]
	
		# stack s0 on top of s1
		return np.vstack((s0, s1))

	def phi_A_sz(self):
		"""
			Construct aspect word () distribution for each (sentiment, topic) combination
			p(A|s, z)
			Return:
				(2 x n_topics) x vocab_size np.array
		"""
		c = 0 # aspect 
		
		# negative sentiment word distribution for all topics
		# the first block of the nzswc matrix
		s0 = self.nzswc[c * self.n_topics : (c + 1) * self.n_topics, :]
		
		# positive sentiment word distribution for all topics
		# the forth block of the nzswc matrix
		s1 = self.nzswc[(self.n_wordTypes * self.n_topics) : (self.n_wordTypes + 1) * self.n_topics, :]
		
		return np.concatenate((s0, s1), axis = 0)

	def phi_O_sz(self):
		"""
			Construct aspect-specific opinion word distribution for each (sentiment, topic) combination
 			p(O|s, z)
			Return:
				(2 x n_topics) x vocab_size np.array
		"""
		c = 1 #aspect-specific 
		
		# negative sentiment word distribution for all topics
		# the second block of the nzswc matrix
		s0 = self.nzswc[c * self.n_topics : (c + 1) * self.n_topics, :]
		
		# positive sentiment word distribution for all topics
		# the fifth block of the nzswc matrix
		s1 = self.nzswc[(self.n_wordTypes + c) * self.n_topics: (self.n_wordTypes + c + 1) * self.n_topics, :]

		return np.concatenate((s0, s1), axis = 0)

	def printTopicDist(self):
		"""
		"""
# print topic distributions of (s,c) pairs
		#for s in range(self.n_sentiments):
		#	for c in range(self.n_wordTypes):
		#		# sum of each block
		#		#print (self.nzswc[(s * self.n_wordTypes + c) * self.n_topics: (s * self.n_wordTypes + c + 1) * self.n_topics, :].sum())
		#		# number of non-zeros in each block
		#		print (np.count_nonzero(self.nzswc[(s * self.n_wordTypes + c) * self.n_topics: (s * self.n_wordTypes + c + 1) * self.n_topics, :]))

# print topic distributions of general opinion words
		#for s in range(self.n_sentiments):
		#	for c in range(self.n_wordTypes):
		#		print (self.nswc[s * self.n_wordTypes + c, :].sum(), np.count_nonzero(self.nswc[s * self.n_wordTypes + c, :]))

# print number of non-zeros in the three matrices
		phi_A = self.phi_A_sz()
		phi_O = self.phi_O_sz()
		phi_G = self.phi_G_s()
		return (np.count_nonzero(phi_A), np.count_nonzero(phi_O), np.count_nonzero(phi_G))

	def run(self, corpus, n_docs, vocab_size, maxiter = 1200):
		"""
			Run collapsed Gibbs sampler.
			return:
				training log (number of non-zeros, etc.)
		"""
		self._initialize(corpus, n_docs, vocab_size)
	
		log = []

		# run the Gibbs sampler 
		for it in range(maxiter):
			# m - doc_id
			# v - words frequencies in the doc
			for m, v in corpus.items():
				# for each word
				for i, w in enumerate(word_indices(v)):
					z = self.topics[(m,i)]
					s = self.sentiments[(m,i)]
					c = self.wordTypes[(m,i)]

					# take out the current word
					self.nz[z] -= 1
					self.ns[s] -= 1
					self.nms[m,s] -= 1
					self.nmzs[m, s * self.n_topics + z] -= 1 
					self.nzsw[s * self.n_topics + z, w] -= 1
					self.nswc[s * self.n_wordTypes + c, w] -= 1
					self.nzswc[(self.n_wordTypes * s + c) * self.n_topics + z, w] -= 1
					
					# joint conditional distribution of (sentiment, topic)
					p_z_s = self._conditional_distribution(m, w)
					
					# flatten the 2 x n_topics matrix to a (2 x n_topics) row vector: [t1, t2, ..., tk, t1, t2, ..., tk]
					resahped_p_z_s = p_z_s.flatten()
					
					sample_ZS = sample_index(resahped_p_z_s)

					# find out which row and column are sampled in p_z_s
					z = sample_ZS % self.n_topics 
					s = math.floor(sample_ZS / self.n_topics)
						
					x = find_word(w)
					p_c = self._wordType_distribution(x, w, s, z)

					c = sample_index(p_c)
					
					self.nz[z] += 1
					self.ns[s] += 1
					self.nms[m,s] += 1
					self.nmzs[m, s * self.n_topics + z] += 1
					self.nzsw[s * self.n_topics + z, w] += 1
					self.nswc[s * self.n_wordTypes + c, w] += 1
					self.nzswc[(self.n_wordTypes * s + c) * self.n_topics + z, w] += 1

					#update 
					self.topics[(m,i)] = z
					self.sentiments[(m,i)] = s
					self.wordTypes[(m,i)] = c
			
			if it % 10 == 0:
				log.append(str(it)  + ":\t" + '\t'.join([str(k) for k in self.printTopicDist()]))
		return log

def retrieve_top_words(dic, phi_G, phi_A, phi_O, n_topics, top_K = 15):
	"""
		Retrieve top words for each topic/sentiment/word_type
	"""
	# find top general sentimental words
	# for each row, find the top 15 entries with highest values
	top_phiGs = phi_G.argsort(axis=1)[:,-top_K:]

	general_pos_opinions = []
	general_neg_opinions = []
	# go through the rows
	for i in range(len(top_phiGs)):
		for word in top_phiGs[i]:
			if phi_G[i, word] != 0:
				if i == 0:
					general_neg_opinions.append(dic[word])
				else:
					general_pos_opinions.append(dic[word])

	# find top aspect words for each sentiment-topic combination
	# for each row, find the top 15 entries with the highest values
	top_phiAsz = phi_A.argsort(axis = 1)[:, -top_K:]
	pos_aspects = []
	neg_aspects = []
	for i in range(len(top_phiAsz)):
		local_list = []		# top words for a particular topic
		# go through a topic
		for word in top_phiAsz[i]:
			if phi_A[i, word] > 0: #np.any(word == phiAszNonZero[i]):
				local_list.append(dic[word])
		
		if i < n_topics:
			neg_aspects.append(local_list)
		else:
			pos_aspects.append(local_list)

	# find top opinion words for each sentiment-topic combination
	top_phiOsz = phi_O.argsort(axis = 1)[:, -top_K:]
	neg_opinions = []
	pos_opinions = []
	for i in range(len(top_phiOsz)):
		local_list = []
		for word in top_phiOsz[i]:
			if phi_O[i, word] > 0:
				local_list.append(dic[word])

		if i < n_topics:
			neg_opinions.append(local_list)
		else:
			pos_opinions.append(local_list)

	return (general_neg_opinions, general_pos_opinions, neg_aspects, pos_aspects, neg_opinions, pos_opinions)

def print_top_g_words(general_neg_opinions, general_pos_opinions, filename):
	"""
		output top words to files
	"""
	with open(filename, 'w') as f:
		f.write('General opinion words in sentiment 0\n')
		f.write('\n'.join(general_neg_opinions))
		f.write('\nGeneral opinion words in sentiment 1\n')
		f.write('\n'.join(general_pos_opinions))

def print_top_a_words(neg_aspects, pos_aspects, filename):
	with open(filename, 'w') as f:
		f.write('Aspect words in sentiment 0\n')
		t = 0
		for l in neg_aspects:
			f.write('\nTopic %d\n' % t)
			f.write('\n'.join(l))
			t += 1
		
		f.write('\nAspect words in sentiment 1\n')
		t = 0
		for l in pos_aspects:
			f.write('\nTopic %d\n' % t)
			f.write('\n'.join(l))
			t += 1

def print_top_o_words(neg_opinions, pos_opinions, filename):
	with open(filename, 'w') as f:
		f.write('Opinion words in sentiment 0\n')
		t = 0
		for l in neg_opinions:
			f.write('\nTopic %d\n' % t)
			f.write('\n'.join(l))
			t += 1
		
		f.write('\nOpinion words in sentiment 1\n')
		t = 0
		for l in pos_opinions:
			f.write('\nTopic %d\n' % t)
			f.write('\n'.join(l))
			t += 1

def print_log(log, filename):
	with open(filename, 'w') as f:
		f.write('\n'.join(log))

if __name__ == "__main__":
	ALPHA = 0.1
	BETA = 0.01
	GAMMA = 1
	N_TOPICS = 15
	N_SENTIMENTS = 2
	N_WORDTYPES = 3

	domain_name = sys.argv[1]

	if not os.path.exists('../results/' + domain_name):
		    os.makedirs('../results/' + domain_name)

	D = {}
	vocab_size = 0
	with open('../data/' + domain_name + '/' + domain_name + '.docs') as f:     
		doc_id = 0
		for line in f:
			D[doc_id] = {}
			words = line.strip().split(' ')

			# each doc can have multiple same words
			for w in words:
				w = int(w)
				if w > vocab_size:
					vocab_size = w
				if w not in D[doc_id]:
					D[doc_id][w] = 1
				else:
					D[doc_id][w] += 1
			doc_id += 1
	
	n_docs = doc_id
	vocab_size += 1

	dic = {}
	with open('../data/' + domain_name + '/' + domain_name + '.vocab') as f:     
		data = f.read().split('\n')
		for line in data: 
			if line != '':
				line = line.split(':')
				dic[int(line[0])] = line[1]
	
	model = JAST(N_TOPICS, N_SENTIMENTS, N_WORDTYPES)
	log = model.run(D, n_docs, vocab_size, maxiter = 21)

	print_log(log, '../results/' + domain_name + '/log.txt')

	phiGS_result = model.phi_G_s()
	phiOsz_result = model.phi_O_sz()
	phiAsz_result = model.phi_A_sz()

	general_neg_opinions, general_pos_opinions, neg_aspects, pos_aspects, neg_opinions, pos_opinions = retrieve_top_words(dic, phiGS_result, phiAsz_result, phiOsz_result, N_TOPICS, 15)
	print_top_g_words(general_neg_opinions, general_pos_opinions, '../results/' + domain_name + '/g_words.txt')
	print_top_a_words(neg_aspects, pos_aspects, '../results/' + domain_name + '/a_words.txt')
	print_top_o_words(neg_opinions, pos_opinions, '../results/' + domain_name + '/o_words.txt')
